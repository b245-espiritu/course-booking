const mongoose = require('mongoose');

const usersSchema = new mongoose.Schema({
    firstName:{
        type:String,
        required:[true, "first name is required!"]
    },
    lastName:{
        type:String,
        required:[true, "last name is required!"]
    },
    email:{
        type:String,
        required:[true, "email address is required!"]
    },
    password:{
        type:String,
        required:[true, "password is required!"]
    },

    isAdmin:{
        type:Boolean,
        default:false
    },
    mobileNo:{
        type:String,
        required:[true, "mobile number is required!"]
    },
    enrollments:[
        {
            courseId:{
                type:String,
                required:[true, "course ID is required!"]
            },
            enrolledOn:{
                type:Date,
                default:new Date()
            },
            status:{
                type:String,
                default:"Enrooled"
            }
        }
    ]

})

module.exports = mongoose.model("User", usersSchema);