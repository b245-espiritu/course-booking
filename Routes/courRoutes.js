const express = require('express')

const router = express.Router();

const courseController = require('../Controllers/courseContoller.js')

const auth = require('../auth.js')


// Route for creating a course

router.post("/addCourse", auth.verify, courseController.addCourse)
router.get("/all", auth.verify, courseController.allCourses)
router.get("/allActiveCourses", courseController.allActiveCourses)
router.get("/allInActiveCourses",auth.verify, courseController.allInActiveCourses)
router.get("/:courseId", courseController.courseDetails)
router.put("/update/:courseId", auth.verify, courseController.updateCourse)
router.put("/courseId/archive/:courseId", auth.verify, courseController.archiveCourse)
module.exports = router;